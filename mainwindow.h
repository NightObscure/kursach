#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGridLayout>
#include <QSignalMapper>
#include "cell.h"

typedef unsigned char byte;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget* parent = nullptr);
    ~MainWindow();

    void setup(int w, int h);
    void gameOver();
    void win();

public slots:
    void pressCell(QString str);
    void setup(QString size);
    void switchMode();

private:
    Ui::MainWindow* ui;
    QGridLayout* layout;
    QSignalMapper* fieldMapper;
    QSignalMapper* changeSizeMapper;
    bool currentMode;
    int minesCount;

    Cell** map;
    int w;
    int h;

    QIcon mineIcon;
    QIcon flagIcon;
};
#endif // MAINWINDOW_H
