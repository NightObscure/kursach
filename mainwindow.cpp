#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtCore>
#include <QRandomGenerator>
#include <QMessageBox>
#include <QPixmap>

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , layout(new QGridLayout)
    , fieldMapper(new QSignalMapper)
    , changeSizeMapper(new QSignalMapper)
    , w(0)
    , h(0)
    , currentMode(true)
{
    ui->setupUi(this);

    ui->field->setLayout(layout);
    connect(ui->modeButton, SIGNAL(clicked()), this, SLOT(switchMode()));

    connect(ui->action5x5, SIGNAL(triggered()), changeSizeMapper, SLOT(map()));
    changeSizeMapper->setMapping(ui->action5x5, "5 5");

    connect(ui->action10x10, SIGNAL(triggered()), changeSizeMapper, SLOT(map()));
    changeSizeMapper->setMapping(ui->action10x10, "10 10");

    connect(ui->action15x15, SIGNAL(triggered()), changeSizeMapper, SLOT(map()));
    changeSizeMapper->setMapping(ui->action15x15, "15 15");

    connect(ui->action30x30, SIGNAL(triggered()), changeSizeMapper, SLOT(map()));
    changeSizeMapper->setMapping(ui->action30x30, "30 30");

    connect(changeSizeMapper, SIGNAL(mapped(QString)), this, SLOT(setup(QString)));

    mineIcon = QIcon(QPixmap(":/icon_mine.png"));
    flagIcon = QIcon(QPixmap(":/icon_flag.png"));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setup(QString size)
{
    QStringList parts = size.split(' ');
    setup(parts[0].toInt(), parts[1].toInt());
}

void MainWindow::setup(int w, int h)
{
    if (this->h > 0)
    {
        if (this->w > 0)
        {
            for(int i = 0; i < this->h; i++)
            {
                delete[] map[i];
            }
        }
        delete[] map;
    }

    this->w = w;
    this->h = h;

    map = new Cell*[h];
    for(int i = 0; i < h; i++)
    {
        map[i] = new Cell[w];
    }

    while (QWidget* w = ui->field->findChild<QWidget*>()) delete w;

    QRandomGenerator random;
    random.seed(time(nullptr));
    minesCount = random.bounded(w * h / 6, w * h / 5);
    ui->totalCounter->display(minesCount);

    for (int i = 0; i < minesCount; i++)
    {
        int mineX = 0;
        int mineY = 0;

        do
        {
            mineX = random.bounded(w);
            mineY = random.bounded(h);
        } while (map[mineY][mineX].code == 255);

        map[mineY][mineX].code = 255;
    }
    for (int y = 0; y < h; y++)
    {
        for (int x = 0; x < w; x++)
        {
            Cell& cell = map[y][x];
            if (cell.code == 255) continue;

            cell.code = 0;
            if (x > 0   && y > 0   && map[y-1][x-1].code == 255) cell.code++;
            if (           y > 0   && map[y-1][x  ].code == 255) cell.code++;
            if (x < w-1 && y > 0   && map[y-1][x+1].code == 255) cell.code++;
            if (x > 0              && map[y  ][x-1].code == 255) cell.code++;
            if (x < w-1            && map[y  ][x+1].code == 255) cell.code++;
            if (x > 0   && y < h-1 && map[y+1][x-1].code == 255) cell.code++;
            if (           y < h-1 && map[y+1][x  ].code == 255) cell.code++;
            if (x < w-1 && y < h-1 && map[y+1][x+1].code == 255) cell.code++;
        }
    }

    for (int y = 0; y < h; y++)
    {
        for (int x = 0; x < w; x++)
        {
            QPushButton* button = new QPushButton();
            button->setSizePolicy(QSizePolicy(QSizePolicy::Policy::MinimumExpanding, QSizePolicy::Policy::MinimumExpanding));
            map[y][x].button = button;
            connect(button, SIGNAL(pressed()), fieldMapper, SLOT(map()));
            fieldMapper->setMapping(button, QString::number(x) + " " + QString::number(y));
            layout->addWidget(button, y, x);
        }
    }

    connect(fieldMapper, SIGNAL(mapped(QString)), this, SLOT(pressCell(QString)));
}

void MainWindow::gameOver()
{
    QMessageBox* mb = new QMessageBox(this);
    mb->setText("Game over!");
    mb->exec();
    setup(w, h);
}

void MainWindow::win()
{
    QMessageBox* mb = new QMessageBox(this);
    mb->setText("You won!");
    mb->exec();
    setup(w, h);
}

void MainWindow::pressCell(QString str)
{
    QStringList coords = str.split(' ');
    int x = coords[0].toInt();
    int y = coords[1].toInt();

    Cell& cell = map[y][x];

    if (currentMode)
    {
        if (cell.code == 255)
        {
            gameOver();
        }
        else
        {
            cell.button->setText(QString::number(cell.code));
            cell.button->setEnabled(false);
        }
    }
    else
    {
        cell.flagged = !cell.flagged;

        cell.button->setIcon(cell.flagged ? flagIcon : QIcon());

        int totalFlagged = 0;
        int correctlyFlagged = 0;
        for (int y = 0; y < h; y++)
        {
            for (int x = 0; x < w; x++)
            {
                if (map[y][x].flagged)
                {
                    totalFlagged++;
                    if (map[y][x].code == 255) correctlyFlagged++;
                }
            }
        }

        ui->currentCounter->display(totalFlagged);
        if (totalFlagged == correctlyFlagged && correctlyFlagged == minesCount)
        {
            win();
        }
    }
}

void MainWindow::switchMode()
{
    currentMode = !currentMode;

    ui->modeButton->setText(QString("Mode: ") + (currentMode ? "open" : "flag"));
}

