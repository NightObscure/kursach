#ifndef CELL_H
#define CELL_H

#include <QPushButton>

typedef unsigned char byte;

struct Cell
{
    QPushButton* button;
    byte code;
    bool flagged;

    Cell()
        : code(0)
        , flagged(false)
    {}
};

#endif // CELL_H
